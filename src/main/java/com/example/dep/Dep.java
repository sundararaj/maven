package com.example.dep;

/**
 * Hello name!
 *
 */
public class Dep
{
        public static void hello(String name){
            System.out.println( "Hello " + name + "!" );
        }


    public static void main( String[] args )
    {
        Dep.hello( "GitLab" );
    }


}